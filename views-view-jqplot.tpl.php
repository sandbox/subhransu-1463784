<?php
// $Id: views-views-jqplot.tpl.php

?>
<?php
$xaxis_field=$options['global']['x-axis'];
$yaxis_field=$options['global']['y-axis'];
$hide_table=$options['global']['hide_table'];
$width=$options['global']['width'];
$height=$options['global']['height'];
?>
<div id="chart1" style="width:<?php echo $width;?>px; height:<?php echo $height;?>px;"></div>
<?php if(!$hide_table): ?>
<table class="<?php print $class; ?>"<?php print $attributes; ?>>
  <?php if (!empty($title)) : ?>
    <caption><?php print $title; ?></caption>
  <?php endif; ?>
  <thead>
    <tr>
      <?php foreach ($header as $field => $label): ?>
        <th class="<?php print $header_classes[$field]; ?>">
          <?php print $label; ?>
        </th>
      <?php endforeach; ?>
    </tr>
  </thead>
  <tbody>
    <?php foreach ($rows as $count => $row): ?>
      <tr class="<?php print implode(' ', $row_classes[$count]); ?>">
        <?php foreach ($row as $field => $content): ?>
          <td class="<?php print $field_classes[$field][$count]; ?>" <?php print drupal_attributes($field_attributes[$field][$count]); ?>>
            <?php print $content; ?>
          </td>
        <?php endforeach; ?>
      </tr>
    <?php endforeach; ?>
  </tbody>
</table>
<?php endif; ?>

<?php 
$data="[";
?>
    <?php 
	foreach ($rows as $count => $row): 
		$data.="[";
		foreach ($row as $key => $value):
				if(strstr($key,$xaxis_field) || strstr($key,$yaxis_field)){
					if (is_numeric(strip_tags($value))) {
						$data.=strip_tags($value).",";
					}
					else{
						$data.="'".strip_tags($value)."',";
					}
				}
		endforeach;
		$data=substr($data,0,-1);
		$data.="],";
	endforeach; 
	$data=substr($data,0,-1);
	$data.="]";
	?>
<?php echo jqplot_prepare_for_graph($options);?>
<script type="text/javascript">
$(document).ready(function(){
	jQuery.noConflict();
	  <?php echo jqplot_prepare_javascript($options,$data); ?>
	});
</script>
