<?php
// $Id: jqplot.views.inc,v 1.1.$

/**
 * @file
 * Views style plugin to render nodes in the jqplot data format.
 *
 * @see views_plugin_style_jqplot.inc
 * @ingroup views_plugins
 */


/**
 * Implementation of hook_views_plugin().
 */
function jqplot_views_plugins() {
  $path = drupal_get_path('module', 'jqplot');
	return array(
    'style' => array( //declare the jqplot style plugin
      'jqplot' => array(
        'title' => t('jqplot Charts'),
        'help' => t('Displays nodes in the jqplot chart format.'),
        'handler' => 'views_plugin_style_jqplot',
        'parent' => 'table',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
		'theme' => 'views_view_jqplot',    
      ),
    )
  );
}
