<?php
/*
* Implementation of views_plugin_style
*
*/

class views_plugin_style_jqplot extends views_plugin_style_table {
 
  /*
   * Set default options
   */
  function options(&$options) {
    parent::options($options);
    $options['char_type'] = '';
  	$options['char_type']['options'] = 'bar';
    $options['global'] = '';
    $options['global']['x-axis'] = '';
    $options['global']['y-axis'] = '';
	$options['global']['x-label'] = '';
    $options['global']['y-label'] = '';
	$options['global']['hide_table'] ;
  	$options['global']['width'] = '600';
  	$options['global']['height'] = '500';
  	$options['global']['appendTitle'] = array();
  	$options['global']['title'] = '';
  }
 /**
   * Provide a form for setting options.
   *
   * @param array $form
   * @param array $form_state
   */ 
  function options_form(&$form, &$form_values) {
    parent::options_form($form, $form_values);
    
  	$field_options = array();
    $fields = $this->display->handler->get_handlers('field');
    foreach ($fields as $id => $handler) {
      $field_options[$id] = $handler->ui_name(FALSE);
    }
    $form['char_type']= array(
  	  '#type' => 'fieldset',
  	  '#title' => t('Chart Type'),
  	  '#collapsible' => FALSE,
  	  '#collapsed' => FALSE,
  	);
  	$form['char_type']['options'] = array(
  	  '#type' => 'radios',
  	  '#title' => t('Options'),
  	  '#description' => t('Graph type'),
  	  '#default_value' => $this->options['char_type']['options'],
  	  '#required' => TRUE,
  	  '#options' => array('bar' => t('Bar'), 'line' => t('Line'), 'pie' => t('Pie')),
  	);
  	$form['global']= array(
  	  '#type' => 'fieldset',
  	  '#title' => t('Options'),
  	  '#collapsible' => FALSE,
  	  '#collapsed' => FALSE,
  	);
    $form['global']['x-axis'] = array(
      '#title' => t('x-axis field'),
      '#description' => t('Field you wan to use in x-axis.'),
      '#type' => 'select',
      '#options' => $field_options,
      '#default_value' => $this->options['global']['x-axis'],
      '#process' => array('views_process_dependency'),
    );
	$form['global']['x-label'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('x-axix Label'),
  	  '#size' => 60,
  	  '#maxlength' => 128,
  	  '#default_value' => $this->options['global']['x-label'] ,
  	);
    $form['global']['y-axis'] = array(
      '#title' => t('y-axis field'),
      '#description' => t('Field you wan to use in y-axis.'),
      '#type' => 'select',
      '#options' => $field_options,
      '#default_value' => $this->options['global']['y-axis'],
      '#process' => array('views_process_dependency'),
    );
	$form['global']['y-label'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('y-axix Label'),
  	  '#size' => 60,
  	  '#maxlength' => 128,
  	  '#default_value' => $this->options['global']['y-label'] ,
  	);
	 $form['global']['hide_table'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show only the graph.'),
    '#description' => t('Hides the table'),
    '#default_value' => $this->options['global']['hide_table'] ,
    );
  	$form['global']['width'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('width'),
  	  '#description' => t('Graph width, defaults to table width'),
  	  '#size' => 5,
  	  '#maxlength' => 5,
  	  '#default_value' => $this->options['global']['width'] ,
  	);
  	$form['global']['height'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('height'),
  	  '#description' => t('Graph height, defaults to table height'),
  	  '#size' => 5,
  	  '#maxlength' => 5,
  	  '#default_value' => $this->options['global']['height'] ,
  	);
   	$form['global']['appendTitle'] = array(
  	  '#type' => 'select',
  	  '#title' => t('appendTitle'),
   	  '#description' => t('Show a title in the graph. "title" field value will be used'),
  	  '#options' => array(
  		True => t('Yes'),
  		False => t('No'),
  	  ),
  	  '#default_value' => $this->options['global']['appendTitle'] ,
  	);
  	$form['global']['title'] = array(
  	  '#type' => 'textfield',
  	  '#title' => t('title'),
  	  '#size' => 60,
  	  '#maxlength' => 128,
  	  '#default_value' => $this->options['global']['title'] ,
  	);
  }
 function options_validate(&$form, &$form_values) {
  }
}
?>